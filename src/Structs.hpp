#pragma once
/*
 * Loosely defined structs to put memory offsets to names
 */

#include <inttypes.h>

struct LinePair
{
	uint32_t instLine;
	uint32_t ip;
};

struct CodeBlock
{
	const char *name;
	int unk_1;
	void *globalStrings;
	void *functionStrings;
	void *globalFloats;
	void *functionFloats;
	int codeSize;
	int unk_2;
	uint32_t *code;
	uint32_t refCount;
	uint32_t lineBreakPairCount;
	uint32_t *lineBreakPairs;
	uint32_t breakListSize;
	uint32_t *breakList;
	CodeBlock *nextFile;
	const char *mRoot;
};

struct Namespace
{
	const char *mName;
	const char *mPackage;

	Namespace *mParent;
	Namespace *mNext;
	void *mClassRep;
	unsigned int mRefCountToParent;
	struct Entry
	{
		enum
		{
			GroupMarker = -3,
			OverloadMarker = -2,
			InvalidFunctionType = -1,
			ScriptFunctionType,
			StringCallbackType,
			IntCallbackType,
			FloatCallbackType,
			VoidCallbackType,
			BoolCallbackType
		};

		Namespace *mNamespace;
		//char _padding1[4];
		Entry *mNext;
		const char *mFunctionName;
		signed int mType;
		signed int mMinArgs;
		signed int mMaxArgs;
		const char *mUsage;
		const char *mPackage;
		CodeBlock *mCode;
		unsigned int mFunctionOffset;
		union {
			const char *(*mStringCallbackFunc)(ADDR obj, int argc, const char *argv[]);
			int (*mIntCallbackFunc)(ADDR obj, int argc, const char *argv[]);
			void (*mVoidCallbackFunc)(ADDR obj, int argc, const char *argv[]);
			float (*mFloatCallbackFunc)(ADDR obj, int argc, const char *argv[]);
			bool (*mBoolCallbackFunc)(ADDR obj, int argc, const char *argv[]);
			const char *mGroupName;
		} cb;
	};
	Entry *mEntryList;
	Entry **mHashTable;
	unsigned int mHashSize;
	unsigned int mHashSequence;  ///< @note The hash sequence is used by the autodoc console facility
	///        as a means of testing reference state.
	char *lastUsage;
};
