#include <windows.h>
#include <string>
#include <sstream>

#include "tracy/Tracy.hpp"

#include "TSFuncs.hpp"
#include "Structs.hpp"

#pragma region Globals
#define COLOR_TORQUE     (0xFFFF5555) // Script functions
#define COLOR_ENGINE     (0xFF55FF55) // Engine functions
#define COLOR_ENGINE_ITF (0xFFFFAA55) // Engine script interface functions
#define COLOR_RENDER     (0xFF5555FF) // Rendering-related Engine functions (unused)

Namespace::Entry *CalledEntry = 0; /* Most recent Entry returned from Namespace::lookup */
void *CalledEntryCB = 0; /* CalledEntry's original callback */

std::string ItfArgs("(SimObject *obj, int argc, const char *argv[])");

char ScratchPad[64]; /* Used for putting numbers into a string for ZoneText */

#pragma endregion /* Globals */

#pragma region Engine Functions
BlFunctionDef(char, __thiscall, FileObject__readMemory, int, char *);
#pragma endregion /* Engine Functions */

#pragma region Utility Functions
/*
 * From tge151 source, derefs code string pointers into strings
 */
inline const char *U32toSTE(uint32_t u)
{
	return *((const char **)&u);
}

/*
 * Returns a nicely formatted string of the Entry's function
 */
std::string getFunctionName(Namespace::Entry *ent)
{
	std::stringstream ss;
	if (ent->mPackage && *ent->mPackage)
	{
		ss << "[" << ent->mPackage << "] ";
	}

	if (ent->mNamespace && ent->mNamespace->mName)
	{
		ss << ent->mNamespace->mName << "::";
	}

	ss << ent->mFunctionName;

	return ss.str();
}

/*
 * Returns a nicely formatted string given a package, namespace, and function name
 */
std::string getFunctionName(const char *package, const char *ns, const char *function)
{
	std::stringstream ss;
	if (package && *package)
	{
		ss << "[" << package << "] ";
	}

	if (ns && *ns)
	{
		ss << ns << "::";
	}

	ss << function;

	return ss.str();
}

/*
 * Returns a nicely formatted string of a given argv array.
 * It assumes the given argv array is TS based which means 0 is the name of the
 * name of the function being called. This function ignores it and starts at
 * argv[1].
 */
std::string getArgs(int argc, const char *argv[])
{
	std::stringstream ss;
	ss << "args: ";

	if (argv && argc > 1)
	{
		for (int i = 1; i < argc - 1; i++)
		{
			ss << "\"" << argv[i] << "\", ";
		}

		ss << "\"" << argv[argc - 1] << "\"";
	}
	else
	{
		/* Make it more obvious there are no args rather than let
		   the user think the args were parsed incorrectly */
		ss << "<none>";
	}

	return ss.str();
}

/*
 * This returns the line number of the source file that the instruction
 * originated from.  It's largely copied from tge151 source with a minor
 * bug fix to make it return the correct line.
 */
uint32_t CodeBlock__getLine(CodeBlock *cb, uint32_t ip)
{
	uint32_t line = 0;
	uint32_t min = 0;
	uint32_t max = cb->lineBreakPairCount - 1;
	LinePair *p = (LinePair *)cb->lineBreakPairs;
	uint32_t found = 0;

	if (!cb->lineBreakPairCount || p[min].ip > ip || p[max].ip < ip)
	{
		return line;
	}
	else if (p[min].ip == ip)
	{
		found = min;
	}
	else if (p[max].ip == ip)
	{
		found = max;
	}
	else
	{
		for (;;)
		{
			if (min == max - 1)
			{
				/* The original function always goes with p[min],
				   but really it should check which one matches ip. */
				if (p[min].ip == ip)
				{
					found = min;
				}
				else
				{
					found = max;
				}
				break;
			}

			/* Binary search */
			uint32_t mid = (min + max) >> 1;
			if (p[mid].ip == ip)
			{
				found = mid;
				break;
			}
			else if (p[mid].ip > ip)
			{
				max = mid;
			}
			else
			{
				min = mid;
			}
		}
	}

	return p[found].instLine >> 8;
}

/*
 * Returns the argument names of a function as defined in the source
 * at the given ip location.
 */
std::string CodeBlock__getFunctionArgs(CodeBlock *cb, uint32_t ip)
{
	std::stringstream ss;

	uint32_t argc = cb->code[ip + 5];
	if (argc)
	{
		for (uint32_t i = 0; i < argc; i++)
		{
			if (i > 0)
			{
				ss << ", ";
			}

			const char *arg = U32toSTE(cb->code[ip + 6 + i]);
			if (arg && *arg)
			{
				ss << arg;
			}
			else
			{
				ss << "JUNK";
				break;
			}
		}
	}

	return ss.str();
}

char *tracySourceCallback(void *data, const char *filename, size_t &size)
{
	static const char *foName = "TracySourceFO";
	char *tracyData = 0;
	size = 0;
	
	ADDR fo = 0;
	if ((fo = tsf_FindObject(foName)) == 0)
	{
		tsf_Eval("new FileObject(TracySourceFO);");
		fo = tsf_FindObject(foName);
	}

	if (fo && FileObject__readMemory(fo, (char *)filename))
	{
		size_t fileSize = *(size_t *)(fo + 56);
		char *fileData = *(char **)(fo + 52);
		
		if (fileSize > 0 && fileData)
		{
			size = fileSize;
			/* Have to use tracy_malloc_fast since Tracy will call tracy_free_fast */
			tracyData = (char *)tracy::tracy_malloc_fast(size);
			memcpy(tracyData, fileData, size);
		}
	}

	return tracyData;
}
#pragma endregion /* Utility Functions */

/*
 * The following region has all the detour functions for the different types
 * of engine interface functions
 */
#pragma region Engine Interface Detours
void ts_detour_void(ADDR obj, int argc, const char *argv[])
{
	std::string name = getFunctionName(CalledEntry);
	std::string args = getArgs(argc, argv);
	std::string fnDef = "void " + name + ItfArgs;

	CalledEntry->cb.mVoidCallbackFunc = (tsf_VoidCallback)CalledEntryCB;
	CalledEntry = 0;

	tracy::ScopedZone ___tracy_scoped_zone(0, 0, 0, fnDef.c_str(), fnDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_ENGINE_ITF);
	ZoneText(args.c_str(), args.length());

	((tsf_VoidCallback)CalledEntryCB)(obj, argc, argv);
}

int ts_detour_int(ADDR obj, int argc, const char *argv[])
{
	std::string name = getFunctionName(CalledEntry);
	std::string args = getArgs(argc, argv);
	std::string fnDef = "int " + name + ItfArgs;

	CalledEntry->cb.mIntCallbackFunc = (tsf_IntCallback)CalledEntryCB;
	CalledEntry = 0;

	tracy::ScopedZone ___tracy_scoped_zone(0, 0, 0, fnDef.c_str(), fnDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_ENGINE_ITF);
	ZoneText(args.c_str(), args.length());

	int ret = ((tsf_IntCallback)CalledEntryCB)(obj, argc, argv);

	snprintf(ScratchPad, sizeof(ScratchPad), "return: %d", ret);
	ZoneText(ScratchPad, strlen(ScratchPad));

	return ret;
}

float ts_detour_float(ADDR obj, int argc, const char *argv[])
{
	std::string name = getFunctionName(CalledEntry);
	std::string args = getArgs(argc, argv);
	std::string fnDef = "float " + name + ItfArgs;

	CalledEntry->cb.mFloatCallbackFunc = (tsf_FloatCallback)CalledEntryCB;
	CalledEntry = 0;

	tracy::ScopedZone ___tracy_scoped_zone(0, 0, 0, fnDef.c_str(), fnDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_ENGINE_ITF);
	ZoneText(args.c_str(), args.length());

	float ret = ((tsf_FloatCallback)CalledEntryCB)(obj, argc, argv);

	snprintf(ScratchPad, sizeof(ScratchPad), "return: %f", ret);
	ZoneText(ScratchPad, strlen(ScratchPad));

	return ret;
}

const char *ts_detour_str(ADDR obj, int argc, const char *argv[])
{
	std::string name = getFunctionName(CalledEntry);
	std::string args = getArgs(argc, argv);
	std::string fnDef = "const char *" + name + ItfArgs;

	CalledEntry->cb.mStringCallbackFunc = (tsf_StringCallback)CalledEntryCB;
	CalledEntry = 0;

	tracy::ScopedZone ___tracy_scoped_zone(0, 0, 0, fnDef.c_str(), fnDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_ENGINE_ITF);
	ZoneText(args.c_str(), args.length());

	const char *ret = ((tsf_StringCallback)CalledEntryCB)(obj, argc, argv);

	std::string retTxt = "return: \"" + std::string(ret) + std::string("\"");
	ZoneText(retTxt.c_str(), retTxt.length());

	return ret;
}

bool ts_detour_bool(ADDR obj, int argc, const char *argv[])
{
	std::string name = getFunctionName(CalledEntry);
	std::string args = getArgs(argc, argv);
	std::string fnDef = "bool " + name + ItfArgs;

	CalledEntry->cb.mBoolCallbackFunc = (tsf_BoolCallback)CalledEntryCB;
	CalledEntry = 0;

	tracy::ScopedZone ___tracy_scoped_zone(0, 0, 0, fnDef.c_str(), fnDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_ENGINE_ITF);
	ZoneText(args.c_str(), args.length());

	bool ret = ((tsf_BoolCallback)CalledEntryCB)(obj, argc, argv);

	snprintf(ScratchPad, sizeof(ScratchPad), "return: %s", ret ? "true" : "false");
	ZoneText(ScratchPad, strlen(ScratchPad));

	return ret;
}
#pragma endregion /* Engine Interface Detours */

#pragma region Hooked Functions
/*
 * Using this hook to mark the start of a tick. It's not aligned since hooking
 * the start of the main game loop isn't simple to do. Also, I picked this
 * function to prevent conflicts with other existing DLLs
 */
BlFunctionDef(void, __thiscall, Sim__advanceToTime, void *);
BlFunctionHookDef(Sim__advanceToTime);
void __fastcall Sim__advanceToTimeHook(void *pthis)
{
	FrameMarkNamed("Game Tick");
	return Sim__advanceToTimeOriginal(pthis);
}

/*
 * Namespace::lookup is called right before an engine interface function is
 * called, so it records the original callback and swaps it for the detoured
 * one. This way, it can be profiled and the orignal callback can be restored.
 */
BlFunctionDef(ADDR, __thiscall, Namespace__lookup, ADDR *, unsigned int);
BlFunctionHookDef(Namespace__lookup)
ADDR __fastcall Namespace__lookupHook(ADDR *ns, void *blank, unsigned int name)
{
	ADDR ret = Namespace__lookupOriginal(ns, name);
	Namespace::Entry *ent = ret ? ((Namespace::Entry *)ret) : 0;

	if (ent && CalledEntry == 0)
	{
		void *cbDetour = 0;
		switch (ent->mType)
		{
		case Namespace::Entry::VoidCallbackType:
			cbDetour = ts_detour_void;
			break;
		case Namespace::Entry::IntCallbackType:
			cbDetour = ts_detour_int;
			break;
		case Namespace::Entry::FloatCallbackType:
			cbDetour = ts_detour_float;
			break;
		case Namespace::Entry::StringCallbackType:
			cbDetour = ts_detour_str;
			break;
		case Namespace::Entry::BoolCallbackType:
			cbDetour = ts_detour_bool;
			break;
		}

		if (cbDetour)
		{
			/* It's a union and they're all pointers so just take one */
			CalledEntryCB = (void *)ent->cb.mVoidCallbackFunc;
			ent->cb.mVoidCallbackFunc = (tsf_VoidCallback)cbDetour;
			CalledEntry = ent;
		}
	}

	return ret;
}

/*
 * CodeBlock::exec is called whenever a TS function runs or a script is executed
 * so it can be hooked and profiled pretty easily
 */
BlFunctionDef(const char *, __thiscall, CodeBlock__exec, CodeBlock *, uint32_t, const char *, Namespace *, int, const char **, bool, const char *, int);
BlFunctionHookDef(CodeBlock__exec);
const char *__fastcall CodeBlock__execHook(CodeBlock *obj, void *blank, uint32_t ip, const char *functionName, Namespace *ns, int argc, const char **argv, bool noCalls, const char *packageName, int setFrame)
{
	std::string name = "";
	std::string args = "";
	std::string functionDef = "";
	std::string sourceFile = "";
	uint32_t line = 0;

	/* If name is null the codeblock was likely a result of eval() */
	if (obj->name)
	{
		sourceFile = obj->name;
	}
	else
	{
		sourceFile = "<input>";
	}

	/* Figure out how to display the zone */
	if (functionName)
	{
		name = getFunctionName(packageName, ns ? ns->mName : 0, functionName);
		args = getArgs(argc, argv);
		functionDef = name + std::string("(") + (argv ? CodeBlock__getFunctionArgs(obj, ip) : std::string("")) + std::string(")");
		line = CodeBlock__getLine(obj, ip);

		/* Chances are no line breaks were made so just go to the top of the file */
		if (line == 0)
		{
			line = 1;
		}
	}
	else if (obj->name)
	{
		/* Not inside a function, probably a file */
		name = obj->name;
	}
	else
	{
		/* Not in a file */
		name = "<global scope>";
	}

	tracy::ScopedZone ___tracy_scoped_zone(line, sourceFile.c_str(), sourceFile.length(), functionDef.c_str(), functionDef.length(), name.c_str(), name.length());
	ZoneColor(COLOR_TORQUE);
	ZoneText(args.c_str(), strlen(args.c_str()));

	const char *ret = CodeBlock__execOriginal(obj, ip, functionName, ns, argc, argv, noCalls, packageName, setFrame);

	std::string retTxt = "return: \"" + std::string(ret) + std::string("\"");
	ZoneText(retTxt.c_str(), retTxt.length());

	return ret;
}

/* 
 * Hook for profiling memory allocations.
 * Not entirely sure if this is Memory::alloc, but it's close
 */
BlFunctionDef(int, __thiscall, Memory__alloc, void *);
BlFunctionHookDef(Memory__alloc);
int __fastcall Memory__allocHook(void *pthis)
{
	static tracy::SourceLocationData Memory__allocSrcData = {
		"Memory::alloc",
		// The additional args are only used in debug builds, so the compiler probably removed them
		"static void *Memory::alloc(dsize_t size, bool array, const char* fileName, const U32 line)",
		0,
		0,
		0
	};
	tracy::ScopedZone ___tracy_scoped_zone(&Memory__allocSrcData, true);
	ZoneColor(COLOR_ENGINE);

	int ret = Memory__allocOriginal(pthis);
	TracyAlloc((void *)ret, (size_t)pthis);
	
	snprintf(ScratchPad, sizeof(ScratchPad), "%u bytes", (size_t)pthis);
	ZoneText(ScratchPad, strlen(ScratchPad));

	return (int)ret;
}

/*
 * Hook for profiling memory deallocations.
 * Also not sure if this is actually Memory::free
 */
BlFunctionDef(void, __fastcall, Memory__free, char *);
BlFunctionHookDef(Memory__free);
void __fastcall Memory__freeHook(char *ptr)
{
	static tracy::SourceLocationData Memory__freeSrcData = {
		"Memory::free",
		"static void Memory::free(void* mem, bool array)",
		0,
		0,
		0
	};
	tracy::ScopedZone ___tracy_scoped_zone(&Memory__freeSrcData, true);
	ZoneColor(COLOR_ENGINE);

	TracyFree((void *)ptr);
	Memory__freeOriginal(ptr);
}
#pragma endregion /* Hooked Functions */

#pragma region DLL Loader Functions
bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	BlScanFunctionHex(FileObject__readMemory, "53 8B 5C 24 08 56 8B F1 57 8B 56 04 85 D2 74 0E");

	BlScanFunctionHex(Namespace__lookup, "53 56 8B F1 57 8B 46 24 3B 05 ? ? ? ? 74 05 E8 ? ? ? ? 8B 7C 24 10 33 D2 8B 5E 20 8B C7 C1 E8 02 F7 F3");
	BlScanFunctionHex(Sim__advanceToTime, "55 8B EC 6A FF 68 ? ? ? ? 64 A1 ? ? ? ? 50 83 EC 24 53 56 57 A1 ? ? ? ? 33 C5 50 8D 45 F4 64 A3 ? ? ? ? 89 4D E8 FF 15 ? ? ? ? 89 45 DC 33 D2 33 C0 89 55 EC 33 FF 89 45 F0 33 DB 89 7D D8 89 5D D0 89 45 D4 8B 4D E8 89 45 FC 8B 35");
	BlScanFunctionHex(CodeBlock__exec, "53 8b dc 83 ec 08 83 e4 f0 83 c4 04 55 8b 6b 04 89 6c 24 04 8b ec 6a ff 68 ? ? ? ? 64 a1 00 00 00 00 50 53 81 ec b8 00 00 00");
	BlScanFunctionHex(Memory__alloc, "55 8B EC 83 E4 F8 80 3D ? ? ? ? ? 53 55 56 57 8B F9 75 0D E8 ? ? ? ? 84 C0 0F 85");
	BlScanFunctionHex(Memory__free, "51 53 55 56 8B 35 ? ? ? ? 57 8B F9 F7 D6 23 F7 3B F7 0F 84 ? ? ? ? 8B 0D");

	BlCreateHook(Namespace__lookup);
	BlCreateHook(Sim__advanceToTime);
	BlCreateHook(CodeBlock__exec);
	BlCreateHook(Memory__alloc);
	BlCreateHook(Memory__free);

	BlTestEnableHook(Namespace__lookup);
	BlTestEnableHook(Sim__advanceToTime);
	BlTestEnableHook(CodeBlock__exec);
	BlTestEnableHook(Memory__alloc);
	BlTestEnableHook(Memory__free);

	TracySourceCallbackRegister(tracySourceCallback, 0);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	tsf_Eval("TracySourceFO.delete();");

	BlTestDisableHook(Namespace__lookup);
	BlTestDisableHook(Sim__advanceToTime);
	BlTestDisableHook(CodeBlock__exec);
	BlTestDisableHook(Memory__alloc);
	BlTestDisableHook(Memory__free);

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}
#pragma endregion /* DLL Loader Functions */

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
