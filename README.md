# BL Tracy

This DLL integrates [Tracy](https://github.com/wolfpld/tracy) into Blockland to allow the profiling of TorqueScript. It currently uses version 0.10 of Tracy which you can find [here](https://github.com/wolfpld/tracy/releases/tag/v0.10).

## Features
* Full profiling of all TorqueScript and engine interface functions
* Ability to capture packages, declarations, arguments, and return values
* Ability to view a function's source code
* All the other features Tracy provides

<details open>
	<summary>Media</summary>

![Slayer](https://i.imgur.com/EWHXo6V.png)

![Glass](https://i.imgur.com/hgnzKL6.png)

![Stats](https://i.imgur.com/KrB0CJh.png)

</details>

## Environment variables
Tracy is able to be configured through environment variables.  You can read its [manual](https://github.com/wolfpld/tracy/releases/download/v0.10/tracy.pdf) to look at all of them, but there is one that you should be aware of:

`TRACY_ONLY_LOCALHOST` - This controls if remote clients can connect to the profiler.  By default, they can.  Set it to `1` to only allow local clients.
