# TSFuncs
This is a library for easily making DLLs for Blockland. Look at `test/TestTSFuncs.cpp` for an example of how to use this. Currently works for Blockland r2033.

## Getting Started
The CMake build process is largely dependent on using GCC.  Building on Linux or using MinGW is recommended.
An example of building the library with MinGW is as follows:
```
cmake -G "MinGW Makefiles" -B build
cd build
mingw32-make install
```
This will automatically fetch the MinHook Github repo and build it as well, which TSFuncs relies on for its detours.

By default, the release versions of TSFuncs and MinHook will be built.  Change `CMAKE_BUILD_TYPE` to `Debug` to build the debug versions of the libraries.  Additionally, change `CMAKE_INSTALL_PREFIX` to change where TSFuncs is installed.  The default install path which is defined by CMake will require sudo/admin privileges.  For example, building the debug versions and installing it into `./output/tsfuncs`:
```
cmake -G "MinGW Makefiles" -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="output/tsfuncs"
cd build
mingw32-make install
```

## TSHooks
This is the foundation for TSFuncs.  It's largely copied from [Redo's BlHooks library](https://gitlab.com/Redo0/redblocklandloader).  However, it has been modified to use MinHook instead as it is more performant.  It provides methods to do signature scanning, byte patching, detouring, etc.

## TSFuncs
This serves as the interface between a DLL and TorqueScript.  It provides methods to find objects, define console methods, eval arbitrary TorqueScript, and more.

## TSVector
This is an extension to TSFuncs. It allows you to expose a `Vector<float>` or a `Vector<signed int>` as a global variable in TorqueScript using `tsf_AddVar`. Since it includes the entirety of TGE's Vector and VectorPtr classes, I chose to keep it separate from TSFuncs. Simply include `TSVector.hpp` in your code to use it.
