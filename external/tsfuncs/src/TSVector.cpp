#include "TSFuncs.hpp"
#include "TSVector.hpp"

void tsf_AddVar(const char *name, Vector<signed int> *data)
{
	tsf_AddVarInternal(name, 5, data);
}

void tsf_AddVar(const char *name, Vector<float> *data)
{
	tsf_AddVarInternal(name, 9, data);
}

//-----------------------------------------------------------------------------
// Torque Game Engine
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------

// This code has been modified to be independent of other TGE code.

bool VectorResize(unsigned int *aSize, unsigned int *aCount, void **arrayPtr, unsigned int newCount, unsigned int elemSize)
{
	if (newCount > 0)
	{
		unsigned int blocks = newCount / VectorBlockSize;
		if (newCount % VectorBlockSize)
			blocks++;
		signed int mem_size = blocks * VectorBlockSize * elemSize;
		*arrayPtr = *arrayPtr ? realloc(*arrayPtr,mem_size) :
			malloc(mem_size);

		*aCount = newCount;
		*aSize = blocks * VectorBlockSize;
		return true;
	}

	if (*arrayPtr)
	{
		free(*arrayPtr);
		*arrayPtr = 0;
	}

	*aSize = 0;
	*aCount = 0;
	return true;
}
